from setuptools import setup, find_namespace_packages
setup(
    name='waveshare_epd',
    version='0.0.1',
    url='https://github.com/waveshare/e-Paper/tree/master/RaspberryPi_JetsonNano/python/lib/waveshare_epd',
    author='WaveShare',
    description='A copy of the waveshare_epd library',
    packages=find_namespace_packages(),
    #install_requires=['pil'],
)
