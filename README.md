This is copied from https://github.com/waveshare/e-Paper/tree/master/RaspberryPi_JetsonNano/python

It *does not* include the aarch64 .so file that the Jetson Nano implementation uses for some reason. I'm only using this for Pi projects, so that I can import it like a real module.
